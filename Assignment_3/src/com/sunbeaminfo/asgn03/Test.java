package com.sunbeaminfo.asgn03;

import java.util.Scanner;
import com.sunbeaminfo.asgn03.Date;

public class Test {

	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Day");
		int day = sc.nextInt();
		System.out.println("Enter Month");
		int month = sc.nextInt();
		System.out.println("Enter Year");
		int year = sc.nextInt();
		
		Date d1 = new Date(day,month,year);
		d1.displayDate();
		
		System.out.println("Enter space separated day,month,year");
		d1.setDay(sc.nextInt());
		d1.setMonth(sc.nextInt());
		d1.setYear(sc.nextInt());
		
		d1.displayDate();
		
		
	}

}
