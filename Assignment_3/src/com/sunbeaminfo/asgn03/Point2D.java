package com.sunbeaminfo.asgn03;

public class Point2D {
	double x,y;
	public Point2D() {
		
	}
	public Point2D(double x,double y)
	{
		this.x = x;
		this.y = y;
	}
	public String getDetails(Point2D p)
	{
		return "(" + p.x + ", " + p.y + ")";
	}
	public boolean isEqual(Point2D p1, Point2D p2)
	{	
		if((p1.x == p2.x)&&(p1.y == p2.y))
		return true ;
		else 
			return false;
	}
	public double calculateDistance (Point2D p1, Point2D p2)
	{
		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;
		return Math.sqrt(dx*dx + dy*dy);
	}
}
