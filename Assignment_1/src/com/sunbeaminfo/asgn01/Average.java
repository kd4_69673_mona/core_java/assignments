package com.sunbeaminfo.asgn01;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		double d1 = sc.nextDouble();
		double d2 = sc.nextDouble();
		
		System.out.println((d1+d2)/2);
		sc.close();
	}

}
