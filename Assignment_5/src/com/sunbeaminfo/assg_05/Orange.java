package com.sunbeaminfo.assg_05;

public class Orange extends fruit{
	
	public Orange() {
		this.color = "orange";
		this.weight = 0.2;
		this.isFresh = true;
		this.name = "Orange";
	}
	
	@Override
	public String toString() {
		return  "This Fruit is "+this.name+"It is "+this.color+". Its weight is "+this.weight+".";
	}
	
	@Override
	public String taste() {
		return "sour";
	}
	
	
}