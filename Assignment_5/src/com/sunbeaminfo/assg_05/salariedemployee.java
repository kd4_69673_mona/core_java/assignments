package com.sunbeaminfo.assg_05;

public class salariedemployee extends employee  {
	private double weeklysalary;
	public salariedemployee(String firstname,String lastname,String ssn,double salary)
	{
		super(firstname,lastname,ssn);
		this.setWeeklysalary(salary);
	}
	public double getWeeklysalary() {
		return weeklysalary;
	}
	public void setWeeklysalary(double salary) {
		if(salary >= 0) 
		{
		    this.weeklysalary = salary;
		}
		else
		   throw new IllegalArgumentException("Weekly salary must be>=0.0");
	}
	@Override
	public double earnings() {
		return getWeeklysalary();
	}
	@Override
	public String toString() {
		return String.format("Salariedemployee :%s\n%s: $%,.2f",super.toString(),"weeklysalary",getWeeklysalary());
	}

	
}

