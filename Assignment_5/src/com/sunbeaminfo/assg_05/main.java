package com.sunbeaminfo.assg_05;

public class main {
    public static salariedemployee salariedemployee;
    public static hourlyemployeed hourlyemployeed;
    public static commissionemployee commissionemployee ;
    public static basepluscommissionemployee basepluscommissionemployee;
	public static void main(String[] args) {
		employee[] employees= new employee[4];
		employees[0]= new salariedemployee("mohi","jain","A001",80000.00);
		employees[1]= new salariedemployee("mona","sharma","A002",60000.00);
		employees[2]= new salariedemployee("saylee","pawar","A003",45000.00);
		employees[3]= new salariedemployee("payal","dhanayat","A004",50000.00);
		
		System.out.println("employees processed polymorphically:\n");
		
		for(employee currentemployee : employees)
		{
			System.out.println(currentemployee);
			if(currentemployee instanceof basepluscommissionemployee)
			{
				basepluscommissionemployee employee = (basepluscommissionemployee) currentemployee;
				
				employee.setBasesalary(1.10*employee.getBasesalary());
			}
			System.out.printf("earned $%,.2f\n\n",currentemployee.earnings());
		}
		}
}
	


